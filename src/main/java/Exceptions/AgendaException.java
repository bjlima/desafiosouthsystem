package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class AgendaException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public AgendaException() {
		super("Agenda Exception");
	}
	
	public AgendaException(String message) {
		super(message);
	}

}
