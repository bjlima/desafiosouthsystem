package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class AgendaAlreadyStartedException extends AgendaException{
	
	private static final long serialVersionUID = 1L;

	public AgendaAlreadyStartedException() {
		super("Agenda already started");
	}
	
	public AgendaAlreadyStartedException(String message) {
		super(message);
	}

}
