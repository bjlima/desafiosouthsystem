package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class AgendaNotFoundException extends AgendaException{
	
	private static final long serialVersionUID = 1L;

	public AgendaNotFoundException(String message) {
		super(message);
	}
	
	public AgendaNotFoundException() {
		super("Agenda not found");
	}

}
