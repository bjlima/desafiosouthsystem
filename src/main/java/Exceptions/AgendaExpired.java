package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class AgendaExpired extends AgendaException{
	
	private static final long serialVersionUID = 1L;

	public AgendaExpired() {
		super("Agenda expired");
	}
	
	public AgendaExpired(String message) {
		super(message);
	}

}
