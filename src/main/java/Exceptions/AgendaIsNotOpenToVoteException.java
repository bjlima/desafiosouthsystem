package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class AgendaIsNotOpenToVoteException extends AgendaException{
	
	private static final long serialVersionUID = 1L;

	public AgendaIsNotOpenToVoteException() {
		super("Agenda is not open to vote");
	}
	
	public AgendaIsNotOpenToVoteException(String message) {
		super(message);
	}

}
