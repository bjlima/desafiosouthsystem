package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class AgendaIsNotClosedException extends AgendaException{
	
	private static final long serialVersionUID = 1L;

	public AgendaIsNotClosedException() {
		super("Agenda is not closed yet");
	}
	
	public AgendaIsNotClosedException(String message) {
		super(message);
	}

}
