package Exceptions;
/**
 * 
 * @author Bruno Lima
 */
public class CPFInvalidException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public CPFInvalidException(String message) {
		super(message);
	}
	
	public CPFInvalidException() {
		super("CPF invalid");
	}

}
