package br.com.southsystem.desafio.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import br.com.southsystem.desafio.enums.AgendaStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgendaResultDTO {

	Long id;
	
	@NotBlank(message="Preencher a descrição")
	String description;
	
	Date createdAt;
	
	Integer totalSim;
	
	Integer totalNao;
	
	AgendaStatus status; 
	
	
}
