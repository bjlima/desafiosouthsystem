package br.com.southsystem.desafio.dto;

import javax.validation.constraints.NotBlank;

import br.com.southsystem.desafio.domain.Agenda;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AgendaDTO {

	Long id;
	
	@NotBlank(message="Preencher a descrição")
	String description;
	
	Integer expiresIn;
	
	public Agenda toAgenda() {
		return new Agenda( id, description, expiresIn );
	}

}
