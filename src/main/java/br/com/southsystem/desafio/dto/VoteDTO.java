package br.com.southsystem.desafio.dto;

import javax.validation.constraints.NotBlank;

import br.com.southsystem.desafio.enums.VoteValues;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VoteDTO {

	@NotBlank(message="Preencher o CPF")
	String CPF;
	
	VoteValues value;
	
}
