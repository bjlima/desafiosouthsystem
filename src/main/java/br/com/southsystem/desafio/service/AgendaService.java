package br.com.southsystem.desafio.service;

import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Exceptions.AgendaAlreadyStartedException;
import Exceptions.AgendaException;
import Exceptions.AgendaIsNotClosedException;
import Exceptions.AgendaIsNotOpenToVoteException;
import Exceptions.AgendaNotFoundException;
import br.com.southsystem.desafio.domain.Agenda;
import br.com.southsystem.desafio.domain.Vote;
import br.com.southsystem.desafio.dto.AgendaDTO;
import br.com.southsystem.desafio.enums.AgendaStatus;
import br.com.southsystem.desafio.enums.VoteValues;
import br.com.southsystem.desafio.repository.AgendaRepository;
import br.com.southsystem.desafio.utils.pubsub.Publisher;

@Service
public class AgendaService {

	@Autowired
	AgendaRepository agendaRepository;
	
	@Autowired
	Publisher publisher;
	
	private Agenda findAgenda(Long id) throws AgendaNotFoundException {
		
		try {

			return agendaRepository.findById(id).get();
		
		} catch(Exception e) {
			
			throw new AgendaNotFoundException();
			
		}

	}
	
	public Agenda save(AgendaDTO agendaDTO) {
		
		ModelMapper modelMapper = new ModelMapper();
		
		return agendaRepository.save(modelMapper.map(agendaDTO, Agenda.class));
	}
	
	public Agenda start(Long id, Integer expiresIn) throws AgendaException{
		
		Agenda agenda = findAgenda(id);
		
		if(agenda.getStatus() != AgendaStatus.Aguardando ) {
			throw new AgendaAlreadyStartedException(); 
		}
		
		agenda.setStatus(AgendaStatus.Votando);
		
		agenda.setExpiresIn(expiresIn);
		
		
		return  agendaRepository.save(agenda);//agendaRepository.save(modelMapper.map(agendaDTO, Agenda.class));
	}
	
	public Agenda finish(Long id) throws AgendaException{
		
		Agenda agenda = findAgenda(id);
		
		if(agenda.getStatus() != AgendaStatus.Votando) {
			throw new AgendaIsNotOpenToVoteException("Can't finish this agenda. It is not open to vote or is closed."); 
		}
		
		agenda.setStatus(AgendaStatus.Pontuando);
		
		agendaRepository.save(agenda);
		
		Predicate<Vote> filterSim = vote -> vote.getValue() == VoteValues.Sim;
		
		var simCount = agenda.getVotes().stream().filter(filterSim)
                .collect(Collectors.toList()).size();

		agenda.setTotalSim(simCount);
		
		agenda.setTotalNao(agenda.getVotes().size() - simCount);
		
		agenda.setStatus(AgendaStatus.Fechada);
		
		agenda = agendaRepository.save(agenda);
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(agenda.getDescription() + " is closed!!!\n");
		
		sb.append("=== Votes ===\n");
		
		sb.append(VoteValues.Sim + " " + agenda.getTotalSim() + "\n");
		
		sb.append(VoteValues.Não + " " + agenda.getTotalNao() + "\n");
		
		publisher.publish(sb.toString());
		
		return agenda;
	}
	
	
	public Agenda getAgendaResult(Long id) throws AgendaException{

		Agenda agenda = findAgenda(id);
		
		if(agenda.getStatus() != AgendaStatus.Fechada) {
			
			throw new AgendaIsNotClosedException();
			
		}
		
		return agenda;
		
	}
	
}
