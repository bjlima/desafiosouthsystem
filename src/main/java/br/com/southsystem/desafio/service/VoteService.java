package br.com.southsystem.desafio.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Calendar;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Exceptions.AgendaException;
import Exceptions.AgendaExpired;
import Exceptions.AgendaIsNotOpenToVoteException;
import Exceptions.AgendaNotFoundException;
import Exceptions.CPFInvalidException;
import br.com.southsystem.desafio.domain.Agenda;
import br.com.southsystem.desafio.domain.Vote;
import br.com.southsystem.desafio.dto.VoteDTO;
import br.com.southsystem.desafio.enums.AgendaStatus;
import br.com.southsystem.desafio.repository.AgendaRepository;
import br.com.southsystem.desafio.repository.VoteRepository;
import br.com.southsystem.desafio.utils.CPFValidator;


@Service
public class VoteService {

	@Autowired
	AgendaRepository agendaRepository;
	
	@Autowired
	VoteRepository voteRepository;
	
	private Agenda findAndValidateAgendaForVoting(Long agendaId) throws AgendaException {
		
		Agenda agenda;
		try {
			agenda = agendaRepository.findById(agendaId).get();
		} catch(Exception e) {
			agenda = null;
		}
		
		if( agenda == null) {
			
			throw new AgendaNotFoundException();
			
		}
		
		Calendar limite = (Calendar) agenda.getCreatedAt().clone();
		
		limite.add(Calendar.SECOND, agenda.getExpiresIn());
		
		if(limite.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
		
			throw new AgendaExpired();
			
		}
		
		if(agenda.getStatus() != AgendaStatus.Votando ) {
			
			throw new AgendaIsNotOpenToVoteException();
			
		}
		
		return agenda;
	}
	
	public Vote validateVote (VoteDTO voteDTO) throws CPFInvalidException {
		CPFValidator cpfValidator = new CPFValidator();
		if(!cpfValidator.validate(voteDTO.getCPF())) {
			throw new CPFInvalidException();
		}
		
		ModelMapper modelMapper = new ModelMapper();
		
		return modelMapper.map(voteDTO, Vote.class);
		
	}
	
	public Boolean vote(Long agendaId, VoteDTO voteDTO) throws AgendaException, SQLIntegrityConstraintViolationException, CPFInvalidException {

		Vote vote = validateVote(voteDTO);
		
		Agenda agenda = findAndValidateAgendaForVoting(agendaId);
		
		vote.setAgenda(agenda);

		agenda.getVotes().add(vote);
		
		agendaRepository.save(agenda);
		
		return true;
	}
}
