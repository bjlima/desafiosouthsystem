package br.com.southsystem.desafio.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CPFValidator {
	
	private final String userServiceValidatorUri = "https://user-info.herokuapp.com/users/";

	
	public boolean validate(String cpf) {
		RestTemplate restTemplate = new RestTemplate();
		try {
			System.out.println(userServiceValidatorUri);
			restTemplate.getForObject(userServiceValidatorUri+cpf, String.class);
		} catch (Exception e) {
			return false;
		}
		return true;
		
	}
}
