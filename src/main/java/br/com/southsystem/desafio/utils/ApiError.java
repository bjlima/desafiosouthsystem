package br.com.southsystem.desafio.utils;

import lombok.Data;

@Data
public class ApiError {

    private Integer status;
    private String message;

    public ApiError(Integer status, String message) {
        super();
        this.status = status;
        this.message = message;
    }
}