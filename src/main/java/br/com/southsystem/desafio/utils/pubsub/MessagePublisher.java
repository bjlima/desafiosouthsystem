package br.com.southsystem.desafio.utils.pubsub;

public interface MessagePublisher {
	
	void publish(String message);
	
}