package br.com.southsystem.desafio.domain;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;

import br.com.southsystem.desafio.enums.VoteValues;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(
	    uniqueConstraints=
	        @UniqueConstraint(columnNames={"agenda_id", "CPF"})
	)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Vote {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	String CPF;
	
	@ManyToOne
	@JoinColumn(name = "agenda_id")
	Agenda agenda;
	
	VoteValues value;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar votedDate;
	
	
	public Vote(Agenda agenda, VoteValues value) {
		this.agenda = agenda;
		this.value = value;
				
	}
	
}
