package br.com.southsystem.desafio.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.southsystem.desafio.enums.AgendaStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Agenda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	String description;
	
	Integer expiresIn;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar createdAt;
	
	@JsonIgnore
	@OneToMany(mappedBy = "agenda", cascade = CascadeType.ALL)
	private List<Vote> votes = new ArrayList<Vote>();
	
	AgendaStatus status = AgendaStatus.Aguardando;
	
	Integer totalSim;
	
	Integer totalNao;
	
	@Version
	private long version;
	
	public Agenda(Long id, String description, Integer expiresIn) {
		
		super();
		
		this.setId(id);
		
		this.setDescription(description);
		
		this.setExpiresIn(expiresIn);
	}

}
