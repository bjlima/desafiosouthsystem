package br.com.southsystem.desafio.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Exceptions.AgendaException;
import br.com.southsystem.desafio.domain.Agenda;
import br.com.southsystem.desafio.dto.AgendaDTO;
import br.com.southsystem.desafio.dto.AgendaResultDTO;
import br.com.southsystem.desafio.repository.AgendaRepository;
import br.com.southsystem.desafio.service.AgendaService;
import br.com.southsystem.desafio.utils.ApiError;

@RestController
@RequestMapping("v1/agenda")
public class AgendaController {

	@Autowired
	AgendaRepository agendaRepository;
	
	@Autowired
	AgendaService agendaService;
	
	Logger logger = LogManager.getLogger(getClass());

	@GetMapping
	public List<Agenda> getAllAgendas() {

		return agendaRepository.findAll();
		
	}
	
	@PostMapping
	public ResponseEntity<Agenda> addAgenda(@Valid @RequestBody AgendaDTO agendaDTO) {
		
		return ResponseEntity.ok(agendaService.save(agendaDTO));
		
	}
	
	@PostMapping("/{id}/start")
	public ResponseEntity<Object> openSessionOfAgenda(@PathVariable Long id, @RequestParam(defaultValue="60") Integer expiresIn) {
		
		try {
			
			return ResponseEntity.ok(agendaService.start(id, expiresIn));
			
		} catch (AgendaException e) {
			
			logger.info(e.getMessage());
			
			ApiError apiError = new ApiError(403, e.getMessage());
			
			return ResponseEntity.status(403).body(apiError);
			
		}
	}
	
	@PostMapping("/{id}/finish")
	public ResponseEntity<Object> closeSessionOfAgenda(@PathVariable Long id) {
		
		try {
			
			ModelMapper modelMapper = new ModelMapper();
			
			return ResponseEntity.ok(modelMapper.map(agendaService.finish(id), AgendaResultDTO.class));
			
		} catch (AgendaException e) {
			
			logger.info(e.getMessage());
			
			ApiError apiError = new ApiError(403, e.getMessage());
			
			return ResponseEntity.status(403).body(apiError);
			
		}
	}

	@GetMapping("/{id}/result")
	public ResponseEntity<Object> getAgendaResult(@PathVariable Long id) {
		
		try {
			
			ModelMapper modelMapper = new ModelMapper();
			
			return ResponseEntity.ok(modelMapper.map(agendaService.getAgendaResult(id), AgendaResultDTO.class));
			
		} catch (AgendaException e) {
			
			logger.info(e.getMessage());
			
			ApiError apiError = new ApiError(403, e.getMessage());
			
			return ResponseEntity.status(403).body(apiError);
			
		}
	}
	
	
}
