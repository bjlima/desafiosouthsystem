package br.com.southsystem.desafio.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Exceptions.AgendaException;
import Exceptions.CPFInvalidException;
import br.com.southsystem.desafio.dto.VoteDTO;
import br.com.southsystem.desafio.service.VoteService;
import br.com.southsystem.desafio.utils.ApiError;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("v1/vote")
public class VoteController {

	@Autowired
	VoteService voteService;

	Logger logger = LogManager.getLogger(getClass());
	@ApiOperation(value = "Votar em uma pauta")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Voto registrado com sucesso"), 
	    @ApiResponse(code = 401, message = "CPF inválido"),
	    @ApiResponse(code = 403, message = "Pauta não permite votação"),
	})
	@PostMapping("/{agendaId}/vote")
	public ResponseEntity<Object> vote(@PathVariable Long agendaId, @Valid @RequestBody VoteDTO voteDTO) {

		try {
			logger.info("/{agendaId}/vote => agendaId = " + agendaId);
			return ResponseEntity.ok(voteService.vote(agendaId, voteDTO));
			
		} catch (AgendaException e) {
			
			logger.warn(e.getMessage());
			
			ApiError apiError = new ApiError(403, e.getMessage());
			
			return ResponseEntity.status(403).body(apiError);
			
		} catch (CPFInvalidException e) {			
			
			ApiError apiError = new ApiError(401, e.getMessage());
			
			logger.warn(e.getMessage());
			
			return ResponseEntity.status(403).body(apiError);
			
		} catch (Exception e) {
			
			ApiError apiError = new ApiError(204, "Vote already registred for this CPF");
			
			logger.warn(e.getMessage());
			
			return ResponseEntity.status(501).body(apiError);
		}

	}
}
