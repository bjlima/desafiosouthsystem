package br.com.southsystem.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.southsystem.desafio.domain.Vote;

public interface VoteRepository extends JpaRepository<Vote, Long> {

}
