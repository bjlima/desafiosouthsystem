package br.com.southsystem.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.southsystem.desafio.domain.Agenda;

public interface AgendaRepository extends JpaRepository<Agenda, Long> {

}
