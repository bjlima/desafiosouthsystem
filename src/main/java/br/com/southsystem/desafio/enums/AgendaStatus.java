package br.com.southsystem.desafio.enums;

public enum AgendaStatus {
	Aguardando,
	Votando,
	Pontuando,
	Fechada
}
